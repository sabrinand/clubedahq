

$(document).ready(function() {

    const SITE = 'http://clubedahq.test/';
    const API = 'http://paymentss.test/api/';

    //------------------------------------------------
    // Máscaras
    //------------------------------------------------
        $('#inputCpf').mask('999.999.999-99', {reverse: true});
        $('#inputCep').mask('99999-999');
        $('.inputCelular').mask('(99) 99999-9999');
        $('.inputTelefone').mask('(99) 9999-9999');

    //------------------------------------------------
    // Form
    //------------------------------------------------

        // Verifica se o cpf já foi cadastrado
        $("#inputCpf").blur(function(){
            let cpf = $("#inputCpf").val();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: SITE + 'checkcpf/'+ cpf,

                success: function(response) {
                    if(response.response==true) {

                        swal({
                            icon: "error",
                            title: response.message
                        });

                        $("#inputCpf").val('');

                    }
                }
            });
        });

        // Ajax do estado
        let options = '<option value="">Estado</option>';
        $.getJSON('https://servicodados.ibge.gov.br/api/v1/localidades/estados', function (data) {
            $.each(data, function (key, val) {
                options += '<option id="'+ val.id +'" value="' + val.nome + '">' + val.nome + '</option>';
            });

            $('#selectEstado').html(options);

            $('#estado-nome').val();
        });

        // Ajax da cidade
        $('#selectEstado').change(function() {
            let options = '<option value="">Cidade</option>';
            let id_est = $('#selectEstado').children(":selected").attr("id");

            $.getJSON('https://servicodados.ibge.gov.br/api/v1/localidades/estados/'+ id_est +'/municipios', function (data) {
                $.each(data, function (key, val) {
                    options += '<option value="' + val.nome + '">' + val.nome + '</option>';
                });

                $('#selectCidade').html(options);
            });
        });

    //------------------------------------------------
    // Checkout
    //------------------------------------------------
        $('#valida-dados').click(function() {
            let card_holder = $('#card-holder').val();
            let expiration_date = $('#ed-month').val() + '/' + $('#ed-year').val();
            let card_number = $('#card-number').val();
            let card_cvc = $('#cvc').val();
            let amount = $('#amount').val();
            let requester = 'Clube da HQ';

            // Validação dos dados
            if(card_holder.length==0) {
                swal({
                    icon: "error",
                    title: 'Insira o nome do titular!'
                });
            } else if ($('#ed-month').val().length==0 || $('#ed-year').val().length==0) {
                swal({
                    icon: "error",
                    title: 'Insira a data de validade!'
                });
            } else if (card_number.length==0) {
                swal({
                    icon: "error",
                    title: 'Insira o número do cartão!'
                });
            } else if (card_cvc.length==0) {
                swal({
                    icon: "error",
                    title: 'Insira o código de segurança do cartão!'
                });
            } else {
                //Validação do cartão de crédito
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: API + 'charge',
                    async: true,
                    data: { 'card_holder': card_holder,
                            'expiration_date': expiration_date,
                            'card_number': card_number,
                            'card_cvc': card_cvc,
                            'amount': amount,
                            'requester': requester },
                    success: function(data) {
                        $("#form-checkout").ajaxSubmit({url: SITE + 'store', type: 'post'});
                        swal({
                            icon: data.type,
                            title: data.message,
                            text: data.text,
                            button: {
                                text: "Fechar",
                            }}).then(function(isConfirm) {
                                if (isConfirm) {
                                    if (data.response) window.location.href = SITE;
                                }
                        });
                    }
                });
            }
        });
    //------------------------------------------------

});

