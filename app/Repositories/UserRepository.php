<?php

namespace  App\Repositories;

use App\User;
use Illuminate\Support\Facades\Hash;


class UserRepository
{
  protected $modelClass = User::class;

  /**
     * Create a new user instance after a valid registration.
     *
     * @param  object  $data
     * @return \App\User
     */
    public function create(object $userData)
    {
        $user = new User;

        $user->nome = $userData->nome;
        $user->email = $userData->email;
        $user->cpf = $userData->cpf;
        $user->rua = $userData->rua;
        $user->numero = $userData->numero;
        $user->celular = $userData->celular;
        $user->telefone = $userData->telefone;
        $user->complemento = isset($userData->complemento) ? $userData->complemento : '';
        $user->bairro = $userData->bairro;
        $user->cidade = $userData->cidade;
        $user->estado = $userData->estado;
        $user->id_plano = $userData->id_plano;
        $user->save();

        return $user;
    }

}
