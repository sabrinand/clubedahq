<?php

namespace  App\Repositories;

use App\Card;

class CardRepository
{
  protected $modelClass = Card::class;

  /**
     * Create a new user instance after a valid registration.
     *
     * @param  int  $user_id
     * @param  object  $data
     * @return \App\Card
     */
    public function create(int $user_id, object $cardData)
    {
        $card = new Card;
        $card->user_id = $user_id;
        $card->titular_cartao = $cardData->card_holder;
        $card->numero_cartao = $cardData->card_number;
        $card->validade_cartao = $cardData->ed_month . '/' . $cardData->ed_year;
        $card->save();

        return $card;
    }
}







