<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'id', 'nome', 'email', 'cpf', 'rua', 'numero', 'celular', 'telefone', 'complemento', 'bairro', 'cidade', 'estado', 'id_plano', 'numero_cartao', 'titular_cartao', 'validade_cartao', 'cvv_cartao',
    ];
}
