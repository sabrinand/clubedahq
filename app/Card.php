<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';

    protected $fillable = [
        'id', 'user_id', 'numero_cartao', 'titular_cartao', 'validade_cartao'
    ];
}
