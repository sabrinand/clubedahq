<?php

namespace App\Http\Controllers;

use App\User;
use App\Repositories\CardRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;


class CheckoutController extends Controller
{
    /**
     * Validate the first form and then redirect to checkout page
     *
     * @return view
     */
    public function check(UserStoreRequest $request)
    {
        // Dados do usuário
        $dataUser = (object)[
            'nome' => $request->nome,
            'email' => $request->email,
            'cpf' => $request->cpf,
            'rua' => $request->rua,
            'numero' => $request->numero,
            'telefone' => isset($request->telefone) ? $request->telefone : '',
            'celular' => $request->celular,
            'bairro' => $request->bairro,
            'cep' => $request->cep,
            'estado' => $request->estado,
            'cidade' => $request->cidade,
            'complemento' => isset($request->complemento) ? $request->complemento : ''
        ];

        //Dados do plano
        switch ($request->plano) {
            case 1:
                $dataUser->plano_tipo = 'Anual';
                $dataUser->plano_preco = '360,00';
                $dataUser->plano_descricao = 'Pagamento único.';
                $dataUser->id_plano = 1;
                break;

            case 2:
                $dataUser->plano_tipo = 'Mensal';
                $dataUser->plano_preco = '40,00';
                $dataUser->plano_descricao = 'A fatura será enviada antes do 5º dia útil.';
                $dataUser->id_plano = 2;
                break;
        }

        //Inclui os dados de usuário na sessão
        $request->session()->put('dataUser', $dataUser);

        return view('checkout')->with('dados', $dataUser);
    }

    /**
     * Check if CPF is unique
     *
     * @return json
     */
    public function checkCpf(string $cpf, bool $intern = null)
    {
        $user = User::where('cpf', $cpf)->get();

        if (count($user) != 0) {
            $response = [
                'response' => true,
                'message' => 'CPF já cadastrado.'
            ];
        } else {
            $response = [
                'response' => false,
                'message' => 'CPF disponível.'
            ];
        }

        return $intern ? $response['response'] : response()->json($response);
    }

    /**
     * Manage the user storage
     *
     * @return json
     */
    public function store(Request $request, UserRepository $userRepository, CardRepository $cardRepository)
    {
        $dataUser = $request->session()->get('dataUser');

        $response = [
            'response' => false,
            'message' => 'Erro ao realizar o cadastro.'
        ];

        if (!$this->checkCpf($dataUser->cpf, 1)) {
            $user = $userRepository->create($dataUser);
            $card = $cardRepository->create($user->id, $request);

            if ($user && $card) {
                $response = [
                    'response' => true,
                    'message' => 'Cadastro realizado com sucesso.'
                ];
            }
        }


        return response()->json($response);
    }
}
