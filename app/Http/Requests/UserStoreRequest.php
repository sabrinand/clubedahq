<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'cpf' => 'required|unique:users',
            'rua' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cep' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'celular' => 'required',
            'plano' => 'required'
        ];
    }
}
