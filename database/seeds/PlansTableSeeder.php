<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'tipo' => 'Anual',
            'valor' => '360.00',
        ]);

        DB::table('plans')->insert([
            'tipo' => 'Mensal',
            'valor' => '40.00',
        ]);
    }
}
