@extends('layouts.app')

@section('nav_content')
@include('layouts.parts.navcheckout')
@endsection

@section('content')
          <section class="payment-form dark">
            <div class="container">
              <div class="block-heading">
              <h3 class="text-white mb-4 text-uppercase">Resumo</h3>
              </div>
              <form id="form-checkout">

                {{ csrf_field() }}

                <div class="products">
                  <h3 class="title">Dados</h3>
                  <div class="resumo-dados">
                    <p>Nome: <label class="text-black-50">{{ $dados->nome }}</label></p>
                    <p>E-mail: <label class="text-black-50">{{ $dados->email }}</label></p>
                    <p>CPF: <label class="text-black-50">{{ $dados->cpf }}</label></p>
                    <p>Endereço: <label class="text-black-50">{{ $dados->rua }}, {{ $dados->numero }} - {{ $dados->bairro }}, {{ $dados->cidade }} - {{ $dados->estado }}</label></p>
                    <p>Celular: <label class="text-black-50 ml-2">{{ $dados->celular }}</label></p>

                    @if ($dados->telefone)
                    <p>Telefone: <label class="text-black-50 ml-2">{{ $dados->telefone }}</label>
                    </p>
                    @endif
                  </div>

                  <h3 class="title">Checkout</h3>
                  <div class="item">
                    <span class="price">R$ {{ $dados->plano_preco }}</span>
                    <p class="item-name">Plano {{ $dados->plano_tipo }}</p>
                    <p class="item-description">{{ $dados->plano_descricao }}</p>
                    <input type="hidden" name="id_plano" value="{{ $dados->id_plano }}"/>
                  </div>
                  <div class="total">Total
                    <span class="price">R$ {{ $dados->plano_preco }}</span>
                    <input type="hidden" id="amount" value="{{ $dados->plano_preco }}">
                  </div>
                </div>
                <div class="card-details">
                  <h3 class="title">Dados do Cartão de Crédito</h3>
                  <div class="row">
                    <div class="form-group col-md-7 col-sm-12">
                      <label for="card-holder"><b>Nome</b> do Titular</label>
                      <input id="card-holder" name="card_holder" type="text" class="form-control" aria-label="Nome do Titular" aria-describedby="basic-addon1">
                    </div>
                    <div class="form-group col-md-5 col-sm-12">
                      <label for="">Validade</label>
                      <div class="input-group expiration-date">
                        <input type="text" id="ed-month" name="ed_month" class="form-control" placeholder="MM" aria-label="MM" aria-describedby="basic-addon1">
                        <span class="date-separator">/</span>
                        <input type="text" id="ed-year" name="ed_year" class="form-control" placeholder="AA" aria-label="AA" aria-describedby="basic-addon1">
                      </div>
                    </div>
                    <div class="form-group col-md-8 col-sm-12">
                      <label for="card-number"><b>Número</b> do cartão</label>
                      <input id="card-number" name="card_number" type="text" class="form-control" aria-label="Número do Cartão" aria-describedby="basic-addon1">
                    </div>
                    <div class="form-group col-md-4 col-sm-12">
                      <label for="cvc">Código de Segurança</label>
                      <input id="cvc" name="cvv" type="text" class="form-control" aria-label="CVC" aria-describedby="basic-addon1">
                    </div>
                    <div class="form-group col-12">
                      <button type="button" class="btn btn-primary btn-block" id="valida-dados">Continuar</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </section>
@endsection
