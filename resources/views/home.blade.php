@extends('layouts.app')

@section('nav_content')
@include('layouts.parts.nav')
@endsection

@section('content')
          <!-- Header -->
          <header class="masthead">
            <div class="container d-flex align-items-center">
              <div class="mx-auto text-center">
                <h1 class="mx-auto my-0 text-uppercase">Clube da HQ</h1>
                <h2 class="text-white mx-auto mt-2 mb-5 subtitle">DC vs Marvel? Não mais! Aqui você pode ter os dois.</h2>
                <a href="#como-funciona" class="btn btn-primary js-scroll-trigger">Conheça</a>
              </div>
            </div>
          </header>

          <!-- Como Funciona -->
          <section id="como-funciona" class="about-section text-center">
            <div class="container">
              <div class="row">
                <div class="col-lg-5 mx-auto red-box">
                  <h2 class="text-white mb-4 text-uppercase">Como funciona?</h2>
                  <p class="text-white-50">Você paga uma mensalidade e passa a receber, todos os meses, nossos kits de histórias em quadrinhos. As revistas são sempre uma surpresa, e algumas edições são exclusivas para os associados do clube.</p>
                </div>
              </div>
              <img src="imgs/superheros.png" class="img-fluid" alt="">
            </div>
          </section>

          <!-- O Clube -->
          <section id="oclube" class="nplanos-section bg-white">
              <div class="container">
                  <div class="row align-items-center no-gutters mb-4 mb-lg-5">
                      <div class="col-xl-8 col-lg-7">
                          <div class="div-img">
                              <img class="img-fluid mb-3 mb-lg-0" src="imgs/batman.gif" alt="">
                          </div>
                      </div>
                      <div class="col-xl-4 col-lg-5">
                          <div class="featured-text text-center text-lg-left">
                          <h4 class="text-uppercase">O Clube</h4>
                          <p class="text-black-50 mb-0">O objetivo do clube é incentivar a experiência literária presente no universo fantástico dos super heróis, tanto para quem ainda nunca sentiu a emoção de abrir o primeiro quadrinho, tanto para os colecionadores de "gibis". Juntos, e através das HQs, navegamos por universos literários, conhecemos novas histórias e amenizamos a competitividade que cerca o mundo dos quadrinhos há anos, agrupando o que há de melhor na DC Comics e na Marvel.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </section>

          <!-- Nossos Planos -->
          <section id="nossos-planos" class="nplanos-section bg-white">
              <div class="container">
                <!-- Plano Anual -->
                <h2 class=" text-center mb-4 text-uppercase">Nossos planos</h2>
                <div class="row justify-content-center no-gutters mb-5 mb-lg-0">

                  <div class="col-lg-6">
                    <img class="img-fluid" src="imgs/batman-lendo.jpg" alt="">
                  </div>
                  <div class="col-lg-6">
                    <div class="bg-black text-center project">
                      <div class="d-flex">
                        <div class="project-text w-100 my-auto text-center text-lg-left">
                          <h4 class="text-white">Anual</h4>
                          <p class="mb-0 text-white-50">Você pagará, no ato da compra, uma tarifa anual com desconto. Para ganhar o desconto, você se compromete a pagar a assinatura anual do plano. Se você cancelar a assinatura antes dos 12 meses contratados, continuará recebendo os quadrinhos até o término do período pago.</p>
                          <hr class="d-none d-lg-block mb-0 ml-0">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Plano Mensal -->
                <div class="row justify-content-center no-gutters">
                  <div class="col-lg-6">
                    <img class="img-fluid" src="imgs/homem-aranha-lendo.jpg" alt="">
                  </div>
                  <div class="col-lg-6 order-lg-first">
                    <div class="bg-black text-center project">
                      <div class="d-flex">
                        <div class="project-text w-100 my-auto text-center text-lg-right">
                          <h4 class="text-white">Mensal</h4>
                          <p class="mb-0 text-white-50">Você será cobrado mensalmente pela assinatura. É possível cancelar o serviço a qualquer momento sem pagar multa.</p>
                          <hr class="d-none d-lg-block mb-0 mr-0">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </section>

          <!-- Assine -->
          <section id="assine" class="assine-section">
            <div class="container">
              <div class="row">
                <div class="col-md-10 col-lg-8 mx-auto text-center">

                  <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
                  <h2 class="text-white mb-5">Assine um dos planos e viaje de Wakanda a Gotham!</h2>

                  <form id="assineForm" action="checkout" method="post">

                    {{ csrf_field() }}

                    <input type="text" class="form-control mb-3" id="inputNome" name="nome" placeholder="Nome completo" required>

                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <input type="email" class="form-control mb-3" id="inputEmail" name="email" placeholder="E-mail" required>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputCpf" name="cpf" placeholder="CPF" required>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-8 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputEndereco" name="rua" placeholder="Rua" required>
                      </div>
                      <div class="col-md-4 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputNumero" name="numero" placeholder="Nº" required>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputComplemento" name="complemento" placeholder="Complemento">
                      </div>
                      <div class="col-md-4 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputBairro" name="bairro" placeholder="Bairro" required>
                      </div>
                      <div class="col-md-4 col-sm-12">
                        <input type="text" class="form-control mb-3" id="inputCep" name="cep" placeholder="Cep" required>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <select id="selectEstado" name="estado" class="form-control mb-3" required>
                          <option>Estado</option>
                        </select>
                        </div>
                      <div class="col-md-6 col-sm-12">
                        <select id="selectCidade" name="cidade" class="form-control mb-3" required>
                          <option>Cidade</option>
                        </select>
                      </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <input type="text" class="form-control mb-3 inputCelular" name="celular" placeholder="Celular" required>
                        </div>
                      <div class="col-md-6 col-sm-12">
                        <input type="text" class="form-control mb-3 inputTelefone" name="telefone" placeholder="Telefone">
                      </div>
                    </div>

                    <div class="row mb-4">
                      <div class="col-md-6 col-sm-12">
                        <input type="radio" name="plano" value="2" class="inputPlano" checked="checked">
                        <label class="tipo-plano text-white" checked>Plano Mensal - R$ 40,00/mês</label>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <input type="radio" name="plano" value="1" class="inputPlano">
                        <label class="tipo-plano text-white">Plano Anual - R$ 360,00/ano</label>
                      </div>
                    </div>

                    <input type="submit" class="btn btn-primary mx-auto" value="Assine"/>
                  </form>
                </div>
              </div>
            </div>
          </section>
@endsection
