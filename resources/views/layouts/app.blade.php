<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @include('layouts.parts.styles')

    </head>

    @yield('nav_content')

    <body class="@yield('body_classes')">
        @yield('header_content')


        <!-- MAIN -->
        <main>
            @yield('content')
        </main>
        <!-- END MAIN -->

        <!-- FOOTER -->
        <footer class="bg-black small text-center text-white-50">
            <div class="container">
                Copyright &copy; Clube da HQ 2020
            </div>
        </footer>
        <!-- END FOOTER -->
    </body>

        @include('layouts.parts.scripts')
</html>
