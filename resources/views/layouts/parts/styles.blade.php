        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="{{ url('css/reset.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('css/all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('css/grayscale.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('css/payment.css') }}" />


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">