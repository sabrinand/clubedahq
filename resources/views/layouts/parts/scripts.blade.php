		<!-- JS -->
		<script src="{{ url('js/jquery.min.js') }}"></script>
		<script src="{{ url('js/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ url('js/jquery.easing.min.js') }}"></script>
		<script src="{{ url('js/grayscale.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
		<script src="https://sweetalert.js.org/assets/sweetalert/sweetalert.min.js"></script>
		<script src="http://malsup.github.com/jquery.form.js"></script> 
		<script src="{{ url('js/scripts.js') }}"></script>
