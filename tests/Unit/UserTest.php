<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */


    public function testCreateUser()
    {
        $user = \App\User::create([
            'nome' => 'Caio Souza Rodrigues',
            'email' => 'caio@gmail.com',
            'cpf' => '600.287.190-00',
            'rua' => 'Travessa Jerusalém',
            'numero' => '397',
            'telefone' => '(11) 3336-2663',
            'celular' => '(11) 99862-2663',
            'complemento' => 'Ap 10',
            'bairro' => 'Olaria',
            'cidade' => 'Aracaju',
            'estado' => 'Sergipe',
            'id_plano' => 1,
        ]);

        $this->assertDatabaseHas('users', ['cpf'=>$user->cpf]);
    }

    public function testCreateCard()
    {
        $user = \App\User::create([
            'nome' => 'Caio Souza Rodrigues',
            'email' => 'caio@gmail.com',
            'cpf' => '600.287.190-00',
            'rua' => 'Travessa Jerusalém',
            'numero' => '397',
            'telefone' => '(11) 3336-2663',
            'celular' => '(11) 99862-2663',
            'complemento' => 'Ap 10',
            'bairro' => 'Olaria',
            'cidade' => 'Aracaju',
            'estado' => 'Sergipe',
            'id_plano' => 1,
        ]);

        $card = \App\Card::create([
            'user_id' => $user->id,
            'numero_cartao' => '4485298887381057',
            'titular_cartao' => 'Caio S Rodrigues',
            'validade_cartao' => '10/29',
        ]);

        $this->assertDatabaseHas('users', ['cpf'=>$user->cpf]);
        $this->assertDatabaseHas('cards', ['user_id'=> $user->id,
                                           'numero_cartao' => $card->numero_cartao]);
    }
}
